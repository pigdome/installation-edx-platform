#Quick Installation the edX Platform

##Get started with the edX platform, using the MITxVM distribution

###1.Setup Vagrant and Virtual Box.
      Vagrant     ==>http://www.vagrantup.com/
      Virtual Box ==>https://www.virtualbox.org/
###2.Setup directory structure for files
      Run cmd
      $mkdir mitx-vargrant
      $cd mitx-vargrant
      $mkdir data
      $cd data
      Download source code and extract here ==>https://github.com/mitocw/edx4edx_lite
###3.Start VM
      Download mitxvm-edx-platform.box ==>http://tao.punsarn.asia/edx.box 
      Move file edx.box to mitx-vargrant
      Run cmd in mitx-vargrant dir
      vagrant init mitxvm mitxvm-edx-platform-02sep13a.box
      vagrant up
      vagrant ssh
      sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions
      exit
      vagrant reload
      vagrant ssh
###4.View your site!
      http://192.168.42.2 -- LMS
	  http://192.168.42.3 -- CMS (Studio)
	  http://192.168.42.4 -- Preview (Studio)
	  http://192.168.42.5 -- Edge (Studio)